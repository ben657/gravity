﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    public Transform model;

    private void Start()
    {
        GetComponent<Destructible>().OnStartDestruct.AddListener(() =>
        {
            Destroy(model.gameObject);
        });
    }
}
