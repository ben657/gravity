﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Ship targetShip;

    Camera camera;
    public float zoom = 0.0f;
    float height = 0.0f;

    private void Awake()
    {
        camera = GetComponent<Camera>();
        height = transform.position.y;
    }

    public Vector3 GetRight()
    {
        Ray r = camera.ViewportPointToRay(new Vector3(1.0f, 0.5f));
        Plane p = new Plane(Vector3.up, Vector3.zero);
        float distance = 0.0f;
        if (p.Raycast(r, out distance))
        {
            return r.GetPoint(distance);
        }
        return Vector3.zero;
    }

    // Start is called before the first frame update
    void Start()
    {
        zoom = camera.orthographicSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (!targetShip) return;

        Vector3 pos = targetShip.transform.position;
        pos.y = height;
        transform.position = pos;

        camera.orthographicSize = zoom;
    }

    private void OnDrawGizmos()
    {
        if(camera)
            Gizmos.DrawWireSphere(GetRight(), 40.0f);
    }
}
