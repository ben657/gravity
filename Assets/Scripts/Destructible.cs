﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Destructible : MonoBehaviour
{
    public ParticleSystem particles;

    public UnityEvent OnStartDestruct = new UnityEvent();

    IEnumerator WaitForParticles()
    {
        yield return new WaitUntil(() => !particles.isPlaying && particles.particleCount == 0);
        Destroy(gameObject);
    }

    public void Destruct()
    {
        particles.Play();
        OnStartDestruct.Invoke();
        StartCoroutine(WaitForParticles());
    }
}
