﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Destructor : MonoBehaviour
{
    public EventDestructibleParam OnStartDestruct = new EventDestructibleParam();

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.attachedRigidbody)
        {
            Rigidbody body = collision.collider.attachedRigidbody;
            Destructible d = body.GetComponent<Destructible>();
            if (d)
            {
                d.Destruct();
                OnStartDestruct.Invoke(d);
            }
        }
    }
}
