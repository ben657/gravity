﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventDestructibleParam : UnityEvent<Destructible> { }

public class EventShipParam : UnityEvent<Ship> { }

public static class Triggers
{
    public static EventShipParam OnPlayerDeath { get; private set; } = new EventShipParam();

    public static EventShipParam OnShipLowFuel { get; private set; } = new EventShipParam();

    public static EventShipParam OnShipLowOxygen { get; private set; } = new EventShipParam();
}