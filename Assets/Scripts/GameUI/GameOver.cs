﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameOver : MonoBehaviour
{
    public TextMeshProUGUI distText;
    public TextMeshProUGUI fuelText;
    public TextMeshProUGUI oxygenText;
    public TextMeshProUGUI totalText;

    public void Setup(Ship ship)
    {
        distText.text = ship.transform.position.x.ToString("n2");
        fuelText.text = ship.CollectedFuel.ToString("n2");
        oxygenText.text = ship.CollectedOxygen.ToString("n2");
        totalText.text = (ship.transform.position.x + ship.CollectedFuel + ship.CollectedOxygen).ToString("n2");
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }
}
