﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public Ship playerShip;

    public LayerMask selectableMask;

    public ResourceBar orbitBar;
    public ResourceBar fuelBar;
    public ResourceBar oxygenBar;

    public InfoMarker marker;
    public GameOver gameOver;
    public TutorialWindow tut;
    public WarningBorder warning;

    Coroutine currentWarning;

    private void Awake()
    {
        marker.gameObject.SetActive(false);
        gameOver.gameObject.SetActive(false);
        tut.gameObject.SetActive(false);
        warning.gameObject.SetActive(false);

        Triggers.OnPlayerDeath.AddListener(ship =>
        {
            ShowGameOver();
        });

        Triggers.OnShipLowFuel.AddListener(ship => ShowWarningBorder(Color.red, 4.0f, 4.0f));
        Triggers.OnShipLowOxygen.AddListener(ship => ShowWarningBorder(Color.blue, 4.0f, 4.0f));
    }

    public void ShowMarker(string title, string body)
    {
        marker.windowTitle.text = title;
        marker.windowBody.text = body;
        marker.gameObject.SetActive(true);
    }

    public void ShowMarker(Planet planet)
    {
        string compInfo = $"Mass: {planet.Mass.ToString("E2")}\nDensity:{planet.Density.ToString("n2")}";
        string keroseneInfo = $"Kerosene: {(planet.KeroseneRich ? planet.Kerosene.ToString("n0") : "Unavailable")}";
        string oxygenInfo = $"Oxygen: {(planet.OxygenRich ? planet.Oxygen.ToString("n0") : "Unavailable")}";

        ShowMarker(planet.name, $"<b>Composition</b>:\n{compInfo}\n\n<b>Resources</b>:\n{keroseneInfo}\n{oxygenInfo}");
        marker.track = planet.transform;
    }

    public void ShowGameOver()
    {
        marker.gameObject.SetActive(false);
        gameOver.Setup(playerShip);
        gameOver.gameObject.SetActive(true);
    }

    public void ShowTutorial(string title, string body, System.Action callback)
    {
        tut.gameObject.SetActive(true);
        tut.Show(title, body, callback);
    }

    IEnumerator WaitForWarning(float time)
    {
        yield return new WaitForSeconds(time);
        warning.gameObject.SetActive(false);
    }

    public void ShowWarningBorder(Color color, float speed, float time)
    {
        if (currentWarning != null) StopCoroutine(currentWarning);
        warning.gameObject.SetActive(true);
        warning.SetColor(color);
        warning.pulseFrequency = speed;
        currentWarning = StartCoroutine(WaitForWarning(time));
    }

    // Update is called once per frame
    void Update()
    {
        orbitBar.max = 100.0f;
        orbitBar.current = playerShip.OrbitProgress * 100.0f;

        fuelBar.max = playerShip.MaxFuel;
        fuelBar.current = playerShip.Fuel;

        oxygenBar.max = playerShip.MaxOxygen;
        oxygenBar.current = playerShip.Oxygen;

        if(Input.GetMouseButtonDown(0))
        {
            Ray mousePosRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(mousePosRay, out hit, Mathf.Infinity, selectableMask))
            {
                Planet planet = hit.collider.GetComponentInParent<Planet>();
                if (planet)
                    ShowMarker(planet);
            }
        }
    }
}
