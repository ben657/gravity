﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
public class InfoMarker : MonoBehaviour
{
    public RectTransform lead;
    public RectTransform window;

    public TextMeshProUGUI windowTitle;
    public TextMeshProUGUI windowBody;

    public Transform track;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!lead || !window) return;

        Vector3 between = window.position - lead.position;
        lead.up = between.normalized;

        RectTransform canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
        Camera.main.WorldToViewportPoint(window.position);

        Vector2 size = new Vector2(lead.sizeDelta.x, (lead.anchoredPosition - window.anchoredPosition).magnitude);
        lead.sizeDelta = size;

        if(track)
        {
            transform.position = Camera.main.WorldToScreenPoint(track.position);
        }
    }
}
