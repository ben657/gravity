﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[ExecuteInEditMode]
public class ResourceBar : MonoBehaviour
{
    public Image image;
    public TextMeshProUGUI labelText;
    public TextMeshProUGUI progressText;
    public GameObject fillIndicator;

    public float min = 0.0f;
    public float max = 1.0f;
    public float current = 1.0f;

    float lastAmount = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        image.type = Image.Type.Filled;
        image.fillMethod = Image.FillMethod.Horizontal;
    }

    // Update is called once per frame
    void Update()
    {
        image.fillAmount = current / max;
        progressText.text = current.ToString("n0") + "/" + max.ToString("n0");

        labelText.rectTransform.sizeDelta = new Vector2(labelText.preferredWidth, labelText.rectTransform.sizeDelta.y);

        fillIndicator.SetActive(image.fillAmount > lastAmount);
        lastAmount = image.fillAmount;
    }
}
