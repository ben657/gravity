﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialWindow : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI bodyText;

    System.Action currentCallback;

    // Start is called before the first frame update
    public void Show(string title, string body, System.Action callback)
    {
        Time.timeScale = 0.0f;
        titleText.text = title;
        bodyText.text = body;
        currentCallback = callback;
    }

    public void Continue(bool cancel)
    {
        gameObject.SetActive(false);
        Time.timeScale = 1.0f;
        if(!cancel)
            currentCallback();
    }
}
