﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class WarningBorder : MonoBehaviour
{
    float timeOffset = 0.0f;

    public float pulseFrequency = 1.0f;
    
    [SerializeField]
    Image image;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    public void SetColor(Color color)
    {
        image.color = color;
    }

    private void OnEnable()
    {
        timeOffset = Time.time;
    }

    private void Update()
    {
        Color c = image.color;
        c.a = Mathf.Cos((Time.time - timeOffset) * pulseFrequency);
        c.a = (c.a + 1.0f) * 0.5f;
        image.color = c;
    }
}
