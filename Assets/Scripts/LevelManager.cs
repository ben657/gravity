﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public Planet planetPrefab;
    public MassiveBody starPrefab;
    public Rigidbody asteroidPrefab;

    public int starFrequency = 20;
    public float starChance = 0.0f;

    public float asteroidChance = 0.5f;
    public int maxAsteroids = 3;

    public float spawnOffset = 100.0f;
    public Ship playerShip;

    Transform lastBody;
    int planetCount = 0;
    int starCount = 0;

    private void Start()
    {
    }

    public Planet SpawnPlanet()
    {
        Planet planet = Instantiate(planetPrefab);
        Vector3 pos = Vector3.zero;
        if (lastBody)
            pos.x = lastBody.transform.position.x + Random.Range(200.0f, 400.0f);
        else
            pos.x = 300.0f;

        pos.z = Random.Range(-100.0f, 100.0f);
        planet.transform.position = pos;
        planet.name = "Planet " + (++planetCount);

        if(Random.value < asteroidChance)
        {
            int asteroids = Random.Range(1, maxAsteroids + 1);
            for(int i = 0; i < asteroids; i++)
            {
                SpawnAsteroid(planet);
            }
        }

        return planet;
    }

    public MassiveBody SpawnStar()
    {
        MassiveBody star = Instantiate(starPrefab);
        Vector3 pos = Vector3.zero;
        if (lastBody)
            pos.x = lastBody.transform.position.x + Random.Range(100.0f, 300.0f);
        else
            pos.x = 300.0f;

        pos.z = Random.Range(-100.0f, 100.0f);
        star.transform.position = pos;
        star.name = "Star " + (++starCount);

        int asteroids = Random.Range(3, maxAsteroids * 3 + 1);
        for (int i = 0; i < asteroids; i++)
        {
            SpawnAsteroid(star);
        }

        return star;
    }

    public void SpawnAsteroid(MassiveBody orbiting)
    {
        Rigidbody asteroid = Instantiate(asteroidPrefab);

        float angle = Random.Range(0.0f, 360.0f);
        Vector3 direction = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward;
        float distance = Random.Range(orbiting.Radius * 2.0f, orbiting.Radius * 5.0f);
        asteroid.transform.position = orbiting.transform.position + (direction * distance);
        direction = Vector3.Cross(direction, Vector3.up).normalized;
        asteroid.velocity = orbiting.GetOrbitalSpeed(distance) * direction;
    }

    // Update is called once per frame
    void Update()
    {
        if (!lastBody || lastBody.position.x < Camera.main.transform.position.x + spawnOffset)
        {
            if (planetCount > 0 && (planetCount + starCount) % starFrequency == 0 && Random.value < starChance)
                lastBody = SpawnStar().transform;
            else
                lastBody = SpawnPlanet().transform;
        }
    }
}
