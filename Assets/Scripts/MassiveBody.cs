﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassiveBody : MonoBehaviour
{
    protected const float volumeConst = 4.0f / 3.0f * Mathf.PI;

    const float g = 0.0667408f;
    
    public float Radius { get; protected set; }
    public float Density { get; protected set; }
    public float Mass { get; private set; }

    public float MassScale { get; private set; }

    [SerializeField]
    Transform model;
    SphereCollider areaOfEffect;
    [SerializeField]
    Transform gravityVisualiser;

    List<Rigidbody> affectedBodies = new List<Rigidbody>();

    protected virtual void Awake()
    {
        areaOfEffect = GetComponent<SphereCollider>();
        areaOfEffect.radius = 1.5f * Radius * Radius;

        model.localScale = Vector3.one * Radius * 2.0f;
        gravityVisualiser.localScale = Vector3.one * areaOfEffect.radius * 2.0f;

        float volume = volumeConst * Radius * Radius * Radius;
        Mass = volume * Density;
        MassScale = Mass / Star.MaxMass;

        gravityVisualiser.GetComponent<Renderer>().material.SetFloat("Strength", MassScale);
    }

    private void OnTriggerEnter(Collider other)
    {
        Rigidbody otherBody = other.attachedRigidbody;
        if(otherBody)
        {
            affectedBodies.Add(otherBody);
            Ship ship = otherBody.GetComponent<Ship>();
            if (ship)
                ship.AddAffectingBody(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Rigidbody otherBody = other.attachedRigidbody;
        if (otherBody)
        {
            affectedBodies.Remove(otherBody);
            Ship ship = otherBody.GetComponent<Ship>();
            if (ship)
                ship.RemoveAffectingBody(this);
        }
    }

    public float GetOrbitalSpeed(float distance)
    {
        return Mathf.Sqrt((g * Mass) / distance);
    }

    public Vector3 GetForce(Vector3 otherPos, float otherMass)
    {
        Vector3 between = transform.position - otherPos;

        float distance2 = between.sqrMagnitude;
        float force = g * Mass * otherMass / distance2;

        return force * between.normalized;
    }

    public float GetOrbitDistance()
    {
        return Radius * 4.0f;
    }

    private void FixedUpdate()
    {
        List<Rigidbody> toRemove = new List<Rigidbody>();
        foreach(Rigidbody otherBody in affectedBodies)
        {
            if (!otherBody)
            {
                toRemove.Add(otherBody);
                break;
            }
            otherBody.AddForce(GetForce(otherBody.transform.position, otherBody.mass));
        }

        toRemove.ForEach(r => affectedBodies.Remove(r));
    }
}
