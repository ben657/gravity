﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MassiveBody
{
    const float minRadius = 10.0f;
    const float maxRadius = 30.0f;

    const float minDensity = 50.0f;
    const float maxDensity = 100.0f;

    public const float MaxMass = volumeConst * maxRadius * maxRadius * maxRadius * maxDensity;

    const float keroseneRarity = 0.8f;
    const float maxKerosene = 1500.0f;

    const float oxygenRarity = 0.5f;
    const float maxOxygen = 1500.0f;

    float initialKerosene = 0.0f;
    public float Kerosene { get; private set; } = 0.0f;
    public bool KeroseneRich { get { return initialKerosene > 0.0f && Kerosene > 0.0f; } }

    float initialOxygen = 0.0f;
    public float Oxygen { get; private set; } = 0.0f;
    public bool OxygenRich { get { return initialOxygen > 0.0f && Oxygen > 0.0f; } }

    public GameObject atmosphere;
    public Color keroseneCol;
    public Color oxygenCol;

    protected override void Awake()
    {
        Radius = Random.Range(minRadius, maxRadius);
        Density = Random.Range(minDensity, maxDensity);

        base.Awake();

        float planetMassScale = Mass / MaxMass;

        Kerosene = Random.value > keroseneRarity ? Random.Range(100.0f, maxKerosene) * planetMassScale : 0.0f;
        initialKerosene = Kerosene;

        Oxygen = Random.value > oxygenRarity ? Random.Range(100.0f, maxOxygen) * planetMassScale : 0.0f;
        initialOxygen = Oxygen;

        if (OxygenRich || KeroseneRich)
        {
            Color col = KeroseneRich ? keroseneCol : oxygenCol;
            if (KeroseneRich && OxygenRich) col = keroseneCol * oxygenCol;
            col.a = keroseneCol.a;
            atmosphere.GetComponent<Renderer>().material.SetColor("Col", col);
            atmosphere.transform.localScale = Vector3.one * (GetOrbitDistance() / Radius);
        }
        else
            Destroy(atmosphere);
    }

    public float ExtractKerosene(float amount)
    {
        float actual = Mathf.Min(Kerosene, amount);
        Kerosene -= actual;
        return actual;
    }

    public float ExtractOxygen(float amount)
    {
        float actual = Mathf.Min(Oxygen, amount);
        Oxygen -= actual;
        return actual;
    }
}
