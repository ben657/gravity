﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour
{
    public float zoomSpeed = 10.0f;

    CameraController controller;

    private void Awake()
    {
        controller = GetComponent<CameraController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float scroll = Input.mouseScrollDelta.y;
        controller.zoom += -scroll * Time.deltaTime * zoomSpeed;
    }
}
