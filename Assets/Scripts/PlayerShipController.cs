﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipController : MonoBehaviour
{
    Ship ship;
    public bool HasMoved { get; private set; } = false;

    private void Awake()
    {
        ship = GetComponent<Ship>();
    }

    private void OnDestroy()
    {
        Triggers.OnPlayerDeath.Invoke(ship);
    }

    // Update is called once per frame
    void Update()
    {
        ship.thrust = Input.GetMouseButton(0) ? 1.0f : 0.0f;
        if (!HasMoved && ship.thrust > 0.0f) HasMoved = true;

        if (ship.Oxygen / ship.MaxOxygen < 0.1f) Triggers.OnShipLowOxygen.Invoke(ship);
        if (ship.Fuel / ship.MaxFuel < 0.1f) Triggers.OnShipLowFuel.Invoke(ship);
    }

    private void FixedUpdate()
    {
        Ray mousePosRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane p = new Plane(Vector3.up, Vector3.zero);
        float distance = 0.0f;
        if (p.Raycast(mousePosRay, out distance))
        {
            Vector3 worldMousePos = mousePosRay.GetPoint(distance);
            Vector3 targetDirection = (worldMousePos - ship.transform.position).normalized;
            float amount = 1.0f - Mathf.Clamp01(Vector3.Dot(ship.transform.forward, targetDirection));
            float direction = Vector3.SignedAngle(ship.transform.forward, targetDirection, Vector3.up) > 0.0f ? 1.0f : -1.0f;
            ship.Rotate(amount * direction);
        }
    }
}
