﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Ship : MonoBehaviour
{
    Rigidbody body;
    LineRenderer predictionLine;

    [SerializeField]
    Transform model;

    [field: SerializeField]
    float MaxBoost { set; get; }
    [field: SerializeField]
    float MaxTorque { set; get; }
    [field: SerializeField]
    float MinOrbitTime { set; get; } = 0.0f;

    public float thrust = 0.0f;

    public float MaxFuel { get; private set; } = 200.0f;
    public float Fuel { get; private set; } = 200.0f;
    [SerializeField]
    float baseFuelUsage = 5.0f;
    [SerializeField]
    float baseRefuelEfficiency = 0.1f;
    [SerializeField]
    float baseRefuelSpeed = 5.0f;
    public float CollectedFuel { get; private set; } = 0.0f;

    public float MaxOxygen { get; private set; } = 100.0f;
    public float Oxygen { get; private set; } = 100.0f;
    [SerializeField]
    float baseOxygenUsage = 2.0f;
    [SerializeField]
    float baseReoxygenateEfficiency = 1.0f;
    [SerializeField]
    float baseReoxygenateSpeed = 2.5f;
    public float CollectedOxygen { get; private set; } = 0.0f;

    [SerializeField]
    ParticleSystem thrustParticles;

    public float OrbitProgress { get { return orbitTime / MinOrbitTime; } }
    public bool InOrbit { get { return closestBody && orbitTime >= MinOrbitTime; } }

    List<MassiveBody> affectedBy = new List<MassiveBody>();
    MassiveBody closestBody;

    List<Vector3> predictedPath = new List<Vector3>();
    public int predictionSteps = 20;
    public float predictionTime = 2.0f;
    
    float orbitTime = 0.0f;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        predictionLine = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Destructible>().OnStartDestruct.AddListener(() =>
        {
            body.isKinematic = true;
            body.velocity = Vector3.zero;
            Destroy(model.gameObject);
            predictionLine.positionCount = 0;
            predictionLine.SetPositions(new Vector3[0]);
        });
    }

    public void Boost(float s)
    {
        body.AddForce(transform.forward * MaxBoost * s, ForceMode.Impulse);
    }

    public void Rotate(float direction)
    {
        body.AddTorque(Vector3.up * direction * MaxTorque);
    }

    public void AddAffectingBody(MassiveBody affectingBody)
    {
        affectedBy.Add(affectingBody);
    }

    public void RemoveAffectingBody(MassiveBody affectingBody)
    {
        affectedBy.Remove(affectingBody);
    }

    private void FixedUpdate()
    {
        if(Fuel > 0.0f)
        {
            body.AddForce(transform.forward * MaxBoost * thrust);
            if (thrust > 0.0f)
            {
                Fuel -= baseFuelUsage * Time.fixedDeltaTime * thrust;
                if (Fuel < 0.0f)
                    Fuel = 0.0f;
            }
        }
    }

    private void Update()
    {
        if (thrustParticles.isPlaying && thrust <= 0.0f) thrustParticles.Stop();
        if (!thrustParticles.isPlaying && thrust > 0.0f) thrustParticles.Play();

        Oxygen -= baseOxygenUsage * Time.deltaTime;

        // Find closest massive body
        closestBody = null;
        if(affectedBy.Count > 0)
        {
            closestBody = affectedBy[0];
            if(affectedBy.Count > 1)
            {
                float closestDist2 = Mathf.Infinity;
                foreach(MassiveBody body in affectedBy)
                {
                    float dist2 = (body.transform.position - transform.position).sqrMagnitude;
                    if(dist2 < closestDist2)
                    {
                        closestDist2 = dist2;
                        closestBody = body;
                    }
                }
            }
        }

        if(closestBody)
        {
            float targetOrbit = closestBody.GetOrbitDistance();
            Vector3 between = closestBody.transform.position - transform.position;
            float orbitDistance2 = between.sqrMagnitude;

            if (orbitDistance2 < targetOrbit * targetOrbit)
            {
                orbitTime += Time.deltaTime;
                if (orbitTime > MinOrbitTime) orbitTime = MinOrbitTime;
            }
            else
            {
                orbitTime = 0.0f;
            }
        }
        else
        {
            orbitTime = 0.0f;
        }

        if(InOrbit)
        {
            if (closestBody is Planet)
            {
                Planet planet = (Planet)closestBody;
                if (planet.OxygenRich)
                {
                    float extracted = planet.ExtractOxygen(baseReoxygenateEfficiency * baseReoxygenateSpeed * Time.deltaTime);
                    Oxygen += extracted;
                    CollectedOxygen += extracted;
                }

                if(planet.KeroseneRich)
                {
                    float extracted = planet.ExtractKerosene(baseRefuelEfficiency * baseRefuelSpeed * Time.deltaTime);
                    Fuel += extracted;
                    CollectedFuel += extracted;
                }
            }
        }

        // Update prediction line
        predictedPath.Clear();
        float predictionTimeStep = predictionTime / predictionSteps;
        Vector3 pos = transform.position;
        Vector3 velocity = body.velocity;
        bool cancel = false;
        for(int i = 0; i < predictionSteps && !cancel; i++)
        {
            predictedPath.Add(pos + Vector3.up * 0.1f);
            Collider[] colliders = Physics.OverlapSphere(pos, 0.1f);
            foreach(Collider collider in colliders)
            {
                MassiveBody affectingBody = collider.GetComponentInParent<MassiveBody>();
                if(affectingBody)
                {
                    velocity += (affectingBody.GetForce(pos, body.mass) / body.mass) * predictionTimeStep;
                    if (!collider.isTrigger)
                        cancel = true;
                }
            }
            pos += velocity * predictionTimeStep;
        }

        if(predictionLine)
        {
            predictionLine.positionCount = predictedPath.Count;
            predictionLine.SetPositions(predictedPath.ToArray());
        }
    }

    private void OnDrawGizmos()
    {
        if (predictedPath.Count < 2) return;
        
        for(int i = 1; i < predictedPath.Count; i++)
        {
            Gizmos.DrawLine(predictedPath[i - 1], predictedPath[i]);
        }
    }
}
