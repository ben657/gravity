﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MassiveBody
{
    const float minRadius = 60.0f;
    const float maxRadius = 120.0f;

    const float minDensity = 50.0f;
    const float maxDensity = 70.0f;

    public const float MaxMass = volumeConst * maxRadius * maxRadius * maxRadius * maxDensity;

    protected override void Awake()
    {
        Radius = Random.Range(minRadius, maxRadius);
        Density = Random.Range(minDensity, maxDensity);

        base.Awake();
    }
}
