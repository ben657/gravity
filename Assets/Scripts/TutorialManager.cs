﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public Ship playerShip;
    public GameUI ui;

    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("Tutorial", 0) == 0)
        {
            ShowWelcome();
        }
    }

    void ShowWelcome()
    {
        ui.ShowTutorial("Welcome to GRAVITY", 
            "In this game you're tasked with keeping the crew of your ship " +
            "alive by careful management of your <b>fuel</b> and <b>oxygen</b> supplies " +
            "(indicated at the lower left of your screen)." +
            "\n\n" +
            "Oh and you'll be doing that by slingshotting around and orbiting planets." +
            "\n\n" +
            "Good luck!", 
            () => ShowControlsTutorial());
    }

    void ShowControlsTutorial()
    {
        ui.ShowTutorial("Controls",
            "Your ship will automatically rotate to face your mouse pointer.\n" +
            "Once you're looking where you want to go, hold <b>left click</b> to " +
            "thrust in that direction." +
            "\n\n" +
            "If you want to see some more of your surroundings, you can zoom in and out " +
            "with the scroll wheel." +
            "\n\n" +
            "You will see the path prediction in front of your ship change to show " +
            "where it will be heading. It will curve around planets ahead to show the affects " +
            "of their gravity on your path. Give it a go!",
            () => StartCoroutine(WaitForControls()));
    }

    IEnumerator WaitForControls()
    {
        yield return new WaitUntil(() => playerShip.GetComponent<PlayerShipController>().HasMoved);
        yield return new WaitForSeconds(10.0f);
        ShowOrbitTutorial();
    }

    void ShowOrbitTutorial()
    {
        ui.ShowTutorial("Orbiting",
            "Now that you're moving around, try getting into orbit of a planet.\n" +
            "Just move towards it and you'll see the prediction line curve closer to it." +
            "Thrust at a right angle to your current path to widen it and try to get it " +
            "to go right around the planet." +
            "\n\n" +
            "As you get within the atmosphere of the planet you'll notice the orbit bar " +
            "above your fuel start to fill up. Once this is at 100 you are considered to be " +
            "orbiting the planet." +
            "\n\n" +
            "Give it a go to move on to replenishing your resources!",
            () => StartCoroutine(WaitForOrbit()));
    }

    IEnumerator WaitForOrbit()
    {
        yield return new WaitUntil(() => playerShip.InOrbit);
        yield return new WaitForSeconds(3.0f);
        ShowResourceTutorial();
    }

    void ShowResourceTutorial()
    {
        ui.ShowTutorial("Replenishing resources",
            "By now you should see your oxygen and fuel starting to get low.\n" +
            "To replenish them you'll need to use your orbiting skills.\n" +
            "If you <b>left click</b> on a planet you will see information about it, " +
            "kerosene is used to replenish your fuel and oxygen is for your crew to breath!" +
            "\n\n" +
            "To collect the resources, just get into orbit and your bars should start to fill back up!",
            () => ShowEndTutorial());
    }

    void ShowEndTutorial()
    {
        PlayerPrefs.SetInt("Tutorial", 1);
        ui.ShowTutorial("Enjoy!",
            "And that's it, your score at the end is a combination of how much " +
            "fuel and oxygen you've collected and the distance you've flown, have fun " +
            "and please leave a rating or comment!",
            () => { });
    }
}
